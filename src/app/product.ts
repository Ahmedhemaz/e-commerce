export interface Product {
    Id: number,
    ProductId: string,
    Description: string,
    Name: string,
    ProductPicUrl: string,
    Price: number,
    Quantity:number,
    Stock:number,
    TotalPrice:number
}
