import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb : FormBuilder,private router: Router, private authService:AuthService) { }
    loginForm:FormGroup;

  ngOnInit() {
    this.loginForm = this.fb.group({
      'email': [null, [Validators.required, Validators.email]],
      'password': [null,[Validators.required,Validators.minLength(8),Validators.maxLength(20)]],
    });
  }

  onSubmit(){
    let userObject = {
      email:this.loginForm.value.email,
      password:this.loginForm.value.password
    }    
    console.log(userObject);
    
    if(this.authValidation(userObject.email,userObject.password)){
      this.router.navigate(['']);
    }else{
      console.log('E5l3');
      console.log(this.authService.isLoggedIn());
      
    }
  }

  authValidation(email,password){
    let user = JSON.parse(localStorage.getItem("user"));

    if(user.email == email && user.password == password){
      this.authService.authenticated();
      return this.authService.isLoggedIn();
    }
    return this.authService.isLoggedIn();
  }

}
