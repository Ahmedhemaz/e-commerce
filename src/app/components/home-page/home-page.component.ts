import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart.service';
import { Product } from 'src/app/product';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  products:Product[] = [];
  constructor(private cartService:CartService) {}

  ngOnInit() {
    this.getData();
  }

  getData(){
    this.cartService.getProductsData().subscribe(
      (result)=>this.products = result
    )};
  
}
