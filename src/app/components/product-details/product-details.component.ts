import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart.service';
import { ActivatedRoute, Router} from '@angular/router';
import { Product } from 'src/app/product';
import { AddToCartService } from 'src/app/add-to-cart.service';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit {

  product:Product;
  Id:number;
  constructor(private cartService:CartService, private route:ActivatedRoute
              , private router:Router, private addToCartService:AddToCartService) { }

  ngOnInit() {  
    this.route.params.subscribe(params => {
  
    this.getSingleProductData(params['id']);
    });  
  }

  getSingleProductData(Id){
    this.cartService.getProductsData().subscribe(
      (result)=> {this.product = result.find(product => product.Id==Id)}
    );
  }

  addToCart(){
    this.addToCartService.addToCart(this.product)
  }
}
