import { Component, OnInit } from '@angular/core';
import { AddToCartService } from 'src/app/add-to-cart.service';
import { Product } from 'src/app/product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit {

  cartCounter:number= this.cartService.getCartItemsCounter();
  cartItemsArray:Product[]= this.cartService.getCartItemsArray();
  totalSum:number=this.cartService.getCartSum();

  constructor(private cartService:AddToCartService, private router:Router) { }

  ngOnInit() {
    this.cartService.cartCounterEmitter
    .subscribe(
      (counter:number)=>{
        this.cartCounter = counter;
      });
    this.cartService.cartItemsArrayEmitter
    .subscribe(
      (items)=>{
        this.cartItemsArray = items;        
      });
      this.cartService.cartSumEmitter
      .subscribe(
        (sum)=>{
          this.totalSum = sum;
        }
      )
      console.log(this.cartItemsArray);
  }

  removeItemFromCart(productId){
    this.cartService.removeItemFromCart(productId);
  }    
  
  addToCart(productId){
    this.cartService.addToCart(this.cartItemsArray.find(product=> product.Id == productId))
  }

  removeOneItemFromCart(productId){
  this.cartService.removeOneItemFromCart(this.cartItemsArray.find(product=> product.Id == productId));
  }

  checkOut(){
    this.cartService.checkOut();
    this.router.navigate(['']);
  }

}
