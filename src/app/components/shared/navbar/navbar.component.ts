import { Component, OnInit } from '@angular/core';
import { AddToCartService } from 'src/app/add-to-cart.service';
import { Product } from 'src/app/product';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  cartCounter:number= this.cartService.getCartItemsCounter();
  cartItemsArray:Product[]= this.cartService.getCartItemsArray();
  wishListCounter: number = this.cartService.getWishListItemsCounter();
  wishListItemsArray: Product[] = this.cartService.getWishListItemsArray();
  constructor(private cartService:AddToCartService, private authService:AuthService) { }

  ngOnInit() {
    this.emitCartCounter();
    this.emitCartItemsArray();
    this.emitWishListCounter();
    this.emitWishListItemsArray();
  };  
  
  removeItemFromCart(productId){
    this.cartService.removeItemFromCart(productId);
  } 

  removeItemFromWishList(productId){
    this.cartService.removeItemFromWishList(productId);
  }

  logOut(){
    this.authService.logOut();
  }

  private emitCartCounter(){
    this.cartService.cartCounterEmitter
    .subscribe(
      (counter:number)=>{
        this.cartCounter = counter;
      });
  }

  private emitCartItemsArray(){
    this.cartService.cartItemsArrayEmitter
    .subscribe(
      (items)=>{
        this.cartItemsArray = items;        
      });
  }

  private emitWishListCounter(){
    this.cartService.wishListCounterEmitter
    .subscribe(
      (counter:number)=>{
        this.wishListCounter = counter;
      });
  }

  private emitWishListItemsArray(){
    this.cartService.wishListItemsArrayEmitter
      .subscribe(
        (items) => {
          this.wishListItemsArray = items;
        });
  }
  
}
