import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/product';
import { AddToCartService } from 'src/app/add-to-cart.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input('Id') Id:number;
  @Input('ProductId') ProductId: string;
  @Input('Description')  Description: string;
  @Input('Name')  Name: string;
  @Input('ProductPicUrl')  ProductPicUrl: string;
  @Input('Price')  Price: number;
  @Input('Stock') Stock:number;

  private product:Product;
  constructor(private router:Router, private addToCartService:AddToCartService) { }

  ngOnInit() {
    this.product = {
      Id:this.Id,
      ProductId:this.ProductId,
      Description:this.Description,
      Name:this.Name,
      ProductPicUrl:this.ProductPicUrl,
      Price:this.Price,
      Quantity:0,
      Stock:this.Stock,
      TotalPrice:0
    }
  }

  onClick(){
    this.router.navigate(['/products',this.Id]);
  }
  addToCart(){
    this.addToCartService.addToCart(this.product)
  }

  addToWishList(){
    this.addToCartService.addToWishList(this.product);
  }

}
