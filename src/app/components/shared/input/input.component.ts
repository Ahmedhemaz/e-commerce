import { Component, OnInit ,Input} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

  constructor() { }

  @Input('type') type:string;
  @Input('name') name:string;
  @Input('id') id:string;
  @Input('placeholder') placeholder:string;
  @Input('controlName') formControlChild:FormControl;
  @Input('controlGroup') formGroupChild:FormGroup;

  
  ngOnInit() {
  }

}
