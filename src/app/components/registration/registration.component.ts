import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  constructor(private fb : FormBuilder) { }
  registrationForm:FormGroup;
  ngOnInit() {
    this.registrationForm = this.fb.group({
      name:[null,[Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null,[Validators.required,Validators.minLength(8),Validators.maxLength(20)]]
    });
  }

  onSubmit(){
    let user:User = {
      name:this.registrationForm.value.name,
      email:this.registrationForm.value.email,
      password:this.registrationForm.value.password,
    }

    localStorage.setItem('user', JSON.stringify(user));
  }

}
