import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputComponent } from './components/shared/input/input.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { CardComponent } from './components/shared/card/card.component';
import { SubmitBtnComponent } from './components/shared/submit-btn/submit-btn.component';
import { CartService } from './cart.service';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { AddToCartService } from './add-to-cart.service';
import { CheckOutComponent } from './components/check-out/check-out.component';
import { AuthService } from './auth.service';

@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    RegistrationComponent,
    PageNotFoundComponent,
    HomePageComponent,
    CardComponent,
    SubmitBtnComponent,
    ProductDetailsComponent,
    CheckOutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [CartService,
              AddToCartService,
              AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
