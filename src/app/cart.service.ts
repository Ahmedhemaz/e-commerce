import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../app/product';
import 'rxjs'
import { map } from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class CartService {

  productMappedArray:Product[] = [];
  url = "https://gist.githubusercontent.com/AbdelRahman-Elshafai/8d0add8341dc607c7ac9ab8a8065bfec/raw/ae88f9ba7999d8f5d81570777ff33a717ecb2aef/products.json";

  constructor(private httpClient:HttpClient) { }

  getProductsData(){
    return this.httpClient.get<Product[]>(this.url).pipe(
      map(products=>{
        for(let product of products){
          let productInterFace:Product = {
            Id: product.Id,
            ProductId: product.ProductId,
            Description: product.Description,
            Name: product.Name,
            ProductPicUrl: product.ProductPicUrl,
            Price: product.Price,
            Quantity:0,
            Stock:product.Quantity,
            TotalPrice:0
          }
          if(this.productMappedArray.length < products.length){
            this.productMappedArray.push(productInterFace); 
          }
        }
          return this.productMappedArray;
      })
    )
  }

  
}
