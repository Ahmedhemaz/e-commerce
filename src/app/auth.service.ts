import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }


  private authenticatedFlag:boolean = false;
  
  authenticated(){
    this.authenticatedFlag = true;
  }

  isLoggedIn(){
    return this.authenticatedFlag;
  }

  logOut(){
    this.authenticatedFlag = false;
  }

}
