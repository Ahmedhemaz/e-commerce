import { Injectable, EventEmitter } from '@angular/core';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class AddToCartService {

  constructor() { 
    if(localStorage.getItem('cartItems') != null){
      this.cartItemsArray = JSON.parse(localStorage.getItem('cartItems'));
    }
    if(localStorage.getItem('wishListItems') != null){
      this.wishListItemsArray = JSON.parse(localStorage.getItem('wishListItems'));
    }
    this.updateCartItemsCounter();
    this.updateWishListItemsCounter();
    this.updateLocalStorageAndView();
  }

  private isEmpty:boolean = false;
  private cartItemCounter:number = 0;
  private totalSum:number = 0;
  private cartItemsArray:Product[]=[];
  private wishListItemsArray:Product[]=[];
  private wishListItemsCounter:number = 0;
  cartCounterEmitter = new EventEmitter<number>();
  cartItemsArrayEmitter = new EventEmitter<Product[]>();
  cartSumEmitter = new EventEmitter<number>();
  wishListItemsArrayEmitter = new EventEmitter<Product[]>();
  wishListCounterEmitter = new EventEmitter<number>();

  addToCart(product:Product){
    this.addToCartItemsArray(product);        
    this.updateLocalStorageAndView()
  }

  addToWishList(product:Product){
    this.addToWishListArray(product);
    this.updateLocalStorageAndView();
  }

  isStockEmpty(product:Product){
    return product.Stock == 0 ? true : false;
  }

  getIsEmpty(){
    return this.isEmpty;
  }

  addToCartItemsArray(productItem:Product){
    let itemIndex = this.cartItemsArray.findIndex(Item => Item.Id == productItem.Id);
    if(itemIndex != -1){
      let product = this.cartItemsArray[itemIndex];
      if(this.isStockEmpty(product)){
        this.isEmpty = true;
        alert("Out Of Stock");
        return;
      }
      this.cartItemsArray[itemIndex].Quantity++;
      this.cartItemsArray[itemIndex].Stock--;
    }else{
      productItem.Quantity++;
      productItem.Stock--;
      this.cartItemsArray.push(productItem);
    }
    this.cartItemCounter++;

  }

  addToWishListArray(product:Product){
    let itemIndex = this.wishListItemsArray.findIndex(productItem => productItem.Id == product.Id);    
    if(itemIndex != -1){
      return;
    }
    this.wishListItemsCounter++;
    this.wishListItemsArray.push(product);
  }

  removeItemFromCart(productId:number){
    let product:Product = this.cartItemsArray.find(product => product.Id == productId);
    this.cartItemCounter -= product.Quantity;
    this.totalSum -= product.TotalPrice;
    this.removeItemFromArray(productId,this.cartItemsArray);  
  }

  private removeItemFromArray(productId:number,array:Product[]){
    array.splice(this.cartItemsArray.findIndex(product => product.Id == productId),1);
    this.updateLocalStorageAndView();
  }

  removeItemFromWishList(productId:number){
    this.wishListItemsCounter--;
    this.removeItemFromArray(productId,this.wishListItemsArray);
  }

  removeOneItemFromCart(product:Product){
    if(product.Quantity == 0){
      return;
    }
    product.Quantity--;
    this.cartItemCounter--;
    product.Stock++;
    product.TotalPrice -= product.Price;
    this.updateLocalStorageAndView();
  }

  private updateLocalStorageAndView(){
    this.calculateSingleProductPrice();
    this.calculateTotalSum();
    localStorage.setItem('cartItems', JSON.stringify(this.cartItemsArray));
    localStorage.setItem('wishListItems', JSON.stringify(this.wishListItemsArray));
    this.cartCounterEmitter.emit(this.cartItemCounter);
    this.cartItemsArrayEmitter.emit(this.cartItemsArray);
    this.cartSumEmitter.emit(this.totalSum);
    this.wishListItemsArrayEmitter.emit(this.wishListItemsArray);
    this.wishListCounterEmitter.emit(this.wishListItemsCounter);
  }

  private updateCartItemsCounter(){
    this.cartItemsArray.forEach(element => {
      this.cartItemCounter += element.Quantity;
    });
  }

  private updateWishListItemsCounter(){
    this.wishListItemsCounter = this.wishListItemsArray.length;
  }

  calculateSingleProductPrice(){
    this.cartItemsArray.forEach(product => {
      product.TotalPrice = product.Price * product.Quantity;
    });
  }

  calculateTotalSum(){
    this.totalSum = 0;
    this.cartItemsArray.forEach(product => {
      this.totalSum += product.TotalPrice;
    });
  }

  checkOut(){
    this.cartItemsArray=[];
    this.cartItemCounter=0;
    this.totalSum=0;
    localStorage.clear();
    this.updateLocalStorageAndView();
  }

  getCartSum(){    
    return this.totalSum;
  }

  getCartItemsCounter(){
    return this.cartItemCounter;
  }

  getCartItemsArray(){
    return this.cartItemsArray;
  }

  getWishListItemsCounter(){
    return this.wishListItemsCounter;
  }

  getWishListItemsArray(){
    return this.wishListItemsArray;
  }
  
}
